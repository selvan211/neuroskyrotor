package com.msc.arthur.minddrone;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.neurosky.thinkgear.TGDevice;
import com.neurosky.thinkgear.TGEegPower;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.Date;

import de.greenrobot.event.EventBus;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VisualiseDataFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VisualiseDataFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 *
 * CodeAndMagic/AChartEngineTest · GitHub. 2014. CodeAndMagic/AChartEngineTest · GitHub. [ONLINE] Available at: https://github.com/CodeAndMagic/AChartEngineTest. [Accessed 25 September 2014].
 *
 */
public class VisualiseDataFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private OnFragmentInteractionListener mListener;
    private GraphicalView mChartView;
    private XYMultipleSeriesDataset mDataset;
    private XYMultipleSeriesRenderer mRenderer;

    private static final String TIME = "H:mm:ss";

    private int[] mColors = new int[]{Color.BLUE, Color.RED, Color.GREEN, Color.YELLOW, Color.MAGENTA};
    private String[] mEEGDescription = new String[]{"Delta", "Theta", "Alpha_avg", "Beta_avg", "Gamma_avg"};
    private TimeSeries[] mEEGWaves;
    private XYSeriesRenderer[] mEEGWavesRenderer;
   // private HashMap<Integer, TimeSeries> mSeries;


    //private TimeSeries delta = new TimeSeries("Delta");

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VisualiseDataFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VisualiseDataFragment newInstance(String param1, String param2) {
        VisualiseDataFragment fragment = new VisualiseDataFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private final CountDownTimer mTimer = new CountDownTimer(15 * 60 * 1000, 2000) {
        @Override
        public void onTick(final long millisUntilFinished) {
            //addValue();
            //scrollGraphRight(new Date().getTime());
            //mChartView.repaint();

        }

        @Override
        public void onFinish() {}
    };

    public VisualiseDataFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Making this fragment a subscriber for the EventBus
        //EventBus.getDefault().register(this);


        //TODO Remove if unnecessary

        mDataset = new XYMultipleSeriesDataset();
        mRenderer = new XYMultipleSeriesRenderer();

        mEEGWaves = new TimeSeries[5];
        mEEGWavesRenderer = new XYSeriesRenderer[5];

        //seriesRenderer = getXYSeriesRenderer(Color.BLUE);

        //Customising the Renderer chart
        mRenderer.setAxisTitleTextSize(16);
        mRenderer.setChartTitleTextSize(20);
        mRenderer.setLabelsTextSize(15);
        //mRenderer.setLegendTextSize(15);
        mRenderer.setBackgroundColor(Color.BLACK);
        mRenderer.setApplyBackgroundColor(true);
        mRenderer.setPointSize(3f);
        //mRenderer.setYTitle("Filtered Values");
        //mRenderer.setXTitle("Time");
        //mRenderer.setAxisTitleTextSize(30);
        //mRenderer.setOrientation(XYMultipleSeriesRenderer.Orientation.HORIZONTAL);
        mRenderer.setZoomEnabled(true);
        mRenderer.setInScroll(true);
        mRenderer.setPanEnabled(true, false);
        mRenderer.setClickEnabled(false);
        //mRenderer.setGridColor(Color.LTGRAY);

        //mRenderer.setFitLegend(true);

        /*if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        //View view = inflater.inflate(R.layout.fragment_visualise_data, container, false);
        final FrameLayout view = (FrameLayout) inflater.inflate(R.layout.fragment_visualise_data, container, false);

        mChartView = ChartFactory.getTimeChartView(getActivity().getApplicationContext(), mDataset, mRenderer, TIME);

        view.addView(mChartView,new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT));

        return view;
    }

    private XYSeriesRenderer getXYSeriesRenderer(int color){
        XYSeriesRenderer SeriesRenderer = new XYSeriesRenderer();
        SeriesRenderer.setColor(color);
        SeriesRenderer.setPointStyle(PointStyle.DIAMOND);
        SeriesRenderer.setLineWidth(3);
        SeriesRenderer.setDisplayChartValues(true);
        SeriesRenderer.setShowLegendItem(false);
        SeriesRenderer.setChartValuesTextSize(5f);

        return SeriesRenderer;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //mChannels = new TimeSeries[3];
        //mEEGWavesRenderer = new XYSeriesRenderer[3];

        /*delta.add(0,0);
        mDataset.addSeries(delta);
        mRenderer.addSeriesRenderer(seriesRenderer);*/

        //ToDO github CodeAndMagic
        for(int i = 0; i< mColors.length; i++){
            mEEGWaves[i] = new TimeSeries(mEEGDescription[i]);
            mEEGWavesRenderer[i] = getXYSeriesRenderer(mColors[i]);
            //mSeries.put(1, mEEGWaves[i]);
        }

        mTimer.start();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    //Receives Messages from the EventBus thread
    public void onEvent(Message msg) {
        switch (msg.what) {
            case TGDevice.MSG_RAW_DATA:
                int raw1 = msg.arg1;
                //tv.append("Got raw: " + msg.arg1 + "\n");
                //Log.v("Raw", "Data: " + raw1 + "\n");
                break;
            case TGDevice.MSG_ATTENTION:
                break;
            case TGDevice.MSG_MEDITATION:
                //tv.append("Meditation: " + msg.arg1 + "\n");
                //Log.i("Meditation: ", ""+msg.arg1);
                break;
            case TGDevice.MSG_EEG_POWER:
                TGEegPower ep = (TGEegPower) msg.obj;
                final long now = new Date().getTime();

                addValue(new Date(now+1000*60*5),ep);
                //tv.append("Delta " + ep.delta + " Theta " + ep.theta + " lowAlpha " + ep.lowAlpha + "  lowBeta  " + ep.lowBeta + "  midGamma " + ep.midGamma + "\n");
                //Log.v("EEG", "EEG Power: " + ep.delta + "\n");
                break;
            case TGDevice.MSG_BLINK:
                //	tv.append("Blink: " + msg.arg1 + "\n");
                break;
            case TGDevice.MSG_RAW_COUNT:
                //tv.append("Raw Count: " + msg.arg1 + "\n");
                break;
            case TGDevice.MSG_LOW_BATTERY:
                break;
            case TGDevice.MSG_RAW_MULTI:
                //TGRawMulti rawM = (TGRawMulti)msg.obj;
                //tv.append("Raw1: " + rawM.ch1 + "\nRaw2: " + rawM.ch2);
            default:
                break;
        }
    }


    //Add the EEG power to the Corresponding TimeSeries and Renderer
    private void addValue(Date x,TGEegPower ep){

        //Adding Delta
        //mEEGWaves[0].add(x,ep.delta*1.0);

        //Adding Theta
        //mEEGWaves[1].add(x,ep.theta*1.0);

        //Adding Alpha as Average
        double alphaAvg = ((ep.lowAlpha + ep.highAlpha)/2)*1.0;
        mEEGWaves[2].add(x,alphaAvg);

        //Adding Beta as Average
        double betaAvg = ((ep.lowBeta + ep.highBeta)/2)*1.0;
        //mEEGWaves[3].add(x, betaAvg);

        //Adding Gamma as Average
        double gammaAvg = ((ep.lowGamma + ep.midGamma)/2)*1.0;
        //mEEGWaves[4].add(x, gammaAvg);

        for (int i = 0; i < mEEGWaves.length; i++) {
            mDataset.addSeries(mEEGWaves[i]);
            mRenderer.addSeriesRenderer(mEEGWavesRenderer[i]);
        }

        //delta.add(x,y);
        //mDataset.addSeries(delta);
        scrollGraphRight();
        mChartView.repaint();
    }



    //Code to move Graph as data is added
    private void scrollGraphRight(){
        double maxX = mEEGWaves[2].getMaxX();
        double minX = maxX -10000;
        //Log.i("Maximum Beta", ": " + Double.valueOf(mEEGWaves[3].getMaxY()).longValue());
        //Log.i("Maximum Beta", ": " + mEEGWaves[3].getY(max-1));

        final double[] limits = new double[]{minX, maxX, 0, mRenderer.getYAxisMax()};
        //final double[] limits = new double[]{minX, maxX, 0, mEEGWaves[3].getY(max-1) + 1000};
        //final double[] limits = new double[]{minX, maxX, mEEGWaves[3].getMinY(), mEEGWaves[3].getMaxY()};
        //final double[] limits = new double[]{minX, maxX, mEEGWaves[3].getMinY(), mEEGWaves[3].getMaxY()};
        mRenderer.setRange(limits);
        //Log.v("Time", " :  "+ time);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
