package com.msc.arthur.minddrone;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.neurosky.thinkgear.TGDevice;

import java.util.Locale;

import de.greenrobot.event.EventBus;
import de.yadrone.base.IARDrone;

public class MainDashboardActivity extends FragmentActivity implements ActionBar.TabListener, DataLogFragment.OnFragmentInteractionListener, ControlDroneFragment.OnFragmentInteractionListener, VisualiseDataFragment.OnFragmentInteractionListener{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;


    public BluetoothAdapter bluetoothAdapter;
    public TGDevice tgDevice;
    public ActionBar actionBar;
    public Boolean isRawDataOn = false;
    public int poorSignalValue = 200;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dashboard);

        //Keep the screen active at all times
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //Connect to the AR Drone
        initializeDrone();

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(bluetoothAdapter == null){
            //Toast.makeText(this, "Device not found", Toast.LENGTH_LONG).show();
        }else{
            /* create the TG Device */
            //Toast.makeText(this, "Device found", Toast.LENGTH_LONG).show();
            tgDevice = new TGDevice(bluetoothAdapter, handler);


            tgDevice.connect(false);

            //connect(false);
        }

        // Set up the action bar.
        actionBar = getActionBar();
        assert actionBar != null;
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setTitle("Signal: " + poorSignalValue);
        actionBar.setDisplayShowHomeEnabled(false);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }

        //Connect to the ARDrone with YaDrone



    }

    //Initializing the AR Drone
    private void initializeDrone()
    {
        Toast.makeText(this,"Initializing Drone", Toast.LENGTH_SHORT).show();
        YADroneApplication app = (YADroneApplication)getApplication();
        IARDrone drone = app.getARDrone();

        try
        {
            drone.start();
            if(drone.getConfigurationManager().isConnected()){
                Toast.makeText(this,"Connected to Drone", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this,"Cannot find Drone", Toast.LENGTH_LONG).show();
            }

        }
        catch(Exception exc)
        {
            exc.printStackTrace();

            if (drone != null)
                drone.stop();
        }
    }

    //ToDo Added from YaDrone
    /**
     * Upon pressing the BACK-button, the user has to confirm the connection to the drone is taken down.
     */
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)
        {
            new AlertDialog.Builder(this).setMessage("Upon exiting, drone will be disconnected !").setTitle("Exit MindDrone?").setCancelable(false).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    YADroneApplication app = (YADroneApplication)getApplication();
                    IARDrone drone = app.getARDrone();
                    drone.stop();
                    finish();
                }
            }).setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    // User selected Cancel, nothing to do here.
                }
            }).show();

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    /**
     * Handles messages from TGDevice
     * @param menu
     * @return
     */
    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            EventBus.getDefault().postSticky(msg);
            switch (msg.what) {
                case TGDevice.MSG_STATE_CHANGE:
                    switch (msg.arg1) {
                        case TGDevice.STATE_IDLE:
                            break;
                        case TGDevice.STATE_CONNECTING:
                            //androidVersion.setText("Connecting...\n");
                            break;
                        case TGDevice.STATE_CONNECTED:
                            //androidVersion.setText("Connected" + "\t" +wifiInfo.getSSID());
                            tgDevice.start();
                            break;
                        case TGDevice.STATE_NOT_FOUND:
                            //tv.append("Can't find\n");
                            break;
                        case TGDevice.STATE_NOT_PAIRED:
                            //tv.append("not paired\n");
                            break;
                        case TGDevice.STATE_DISCONNECTED:
                            //tv.append("Disconnected\n");
                    }
                    break;
                case TGDevice.MSG_POOR_SIGNAL:
                    poorSignalValue = msg.arg1;
                    //EventBus.getDefault().postSticky(poorSignalValue);
                    actionBar.setTitle("Signal: " + poorSignalValue);
                    //Log.v("Poor", "Signal: " + poorSignalValue + "\n");
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.raw_data_on:
                tgDevice.close();
                tgDevice.connect(true);
                isRawDataOn = true;
                Toast.makeText(this,"Raw Data Activated", Toast.LENGTH_SHORT).show();
                break;
            case R.id.raw_data_off:
                tgDevice.close();
                tgDevice.connect(false);
                isRawDataOn = false;
                Toast.makeText(this,"Raw Data Deactivated", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //Return isRawDataOn value
    public boolean isRawDataOn(){
        return isRawDataOn;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
        //Toast.makeText(getApplicationContext(), tab.getText() + " Selected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Toast.makeText(getApplicationContext(), "Welcome", Toast.LENGTH_SHORT).show();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
                switch (position){
                    //TODO change the parameters of the fragments if they are not needed
                    case 0:
                        return ControlDroneFragment.newInstance("e","e");
                        //return PlaceholderFragment.newInstance(position);
                    case 1:
                        return PlaceholderFragment.newInstance(position);
                        //return VisualiseDataFragment.newInstance("e","e");
                    case 2:
                        return PlaceholderFragment.newInstance(position);
                        //return DataLogFragment.newInstance();

                }
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_tab1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_tab2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_tab3).toUpperCase(l);
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {

            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_main_dashboard, container, false);
        }
    }

}
