package com.msc.arthur.minddrone;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.neurosky.thinkgear.TGDevice;
import com.neurosky.thinkgear.TGEegPower;

import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DataLogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DataLogFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class DataLogFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private String mParam1;

    private TextView tv, androidVersion;
    private ProgressBar progressBarAtt;
    private ProgressBar progressBarMed;
    private ScrollView scrollView;
    private OnFragmentInteractionListener mListener;
    private MainDashboardActivity mainDashboardActivity;
    private int clearScroll = 10000;

    private boolean wasPaused = false;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DataLogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DataLogFragment newInstance() {
        DataLogFragment fragment = new DataLogFragment();
        return fragment;
    }
    public DataLogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //EventBus.getDefault().register(this);
        mainDashboardActivity = new MainDashboardActivity();
        if (getArguments() != null) {
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_data_log, container, false);

        tv = (TextView) view.findViewById(R.id.textView);
        androidVersion = (TextView) view.findViewById(R.id.androidVersion);
        progressBarAtt = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBarMed = (ProgressBar)view.findViewById(R.id.progressBar2);

        //ScrollView making it auto scrollable to the bottom
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                scrollView.post(new Runnable() {
                    public void run() {
                        scrollView.fullScroll(View.FOCUS_DOWN);
                    }
                });
            }
        });

        //scrollView.removeAllViews();

        WifiManager wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        androidVersion.setText("Android Version: " + Build.VERSION.SDK_INT + "  Current SSID: " + wifiInfo.getSSID()  +"\n");

        //androidVersion.setText(""+scrollView.getMeasuredHeightAndState());

       return view;
    }

    //Receives Messages from the EventBus thread
    public void onEvent(Message msg){
        switch (msg.what) {

            case TGDevice.MSG_RAW_DATA:
                int raw1 = msg.arg1;
                tv.append("Got raw: " + msg.arg1 + "\n");
                //Log.v("Raw", "Data: " + raw1 + "\n");
                break;
            case TGDevice.MSG_ATTENTION:
                int att = msg.arg1;
                tv.append("Attention: " + msg.arg1 + "\n");
                //Log.v("HelloA", "Attention: " + att + "\n");
                //if(!mainDashboardActivity.isRawDataOn()){
                    progressBarAtt.setProgress(att);
                //}
                break;
            case TGDevice.MSG_MEDITATION:
                tv.append("Meditation: " + msg.arg1 + "\n");
                    progressBarMed.setProgress(msg.arg1);
                //Log.v("HelloM", "Meditation: " + msg.arg1 + "\n");
                break;
            case TGDevice.MSG_EEG_POWER:
                TGEegPower ep = (TGEegPower)msg.obj;
                tv.append("Delta " +ep.delta + " Theta "+ ep.theta +" lowAlpha "+ ep.lowAlpha + "  lowBeta  "+ep.lowBeta+  "  midGamma "+  ep.midGamma+ "\n");
                //Log.v("EEG", "EEG Power: " + ep.delta + "\n");
                break;
            case TGDevice.MSG_BLINK:
                tv.append("Blink: " + msg.arg1 + "\n");
                Log.v("Blink Metric", ": " + msg.arg1);
                break;
            case TGDevice.MSG_RAW_COUNT:
                //tv.append("Raw Count: " + msg.arg1 + "\n");
                break;
            case TGDevice.MSG_LOW_BATTERY:
                break;
            case TGDevice.MSG_RAW_MULTI:
                //TGRawMulti rawM = (TGRawMulti)msg.obj;
                //tv.append("Raw1: " + rawM.ch1 + "\nRaw2: " + rawM.ch2);
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        wasPaused = true;
        EventBus.getDefault().unregister(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
