package com.msc.arthur.minddrone;

import android.app.Application;

import de.yadrone.base.ARDrone;
import de.yadrone.base.IARDrone;

/**
 * Created by arthur on 8/25/14.
 */
public class YADroneApplication extends Application{
    /**
     * The drone is kept in the application context so that all activities use the same drone instance
     *
     ** YADrone/YADrone/src/de/yadrone/apps/tutorial at master · MahatmaX/YADrone · GitHub. 2014. YADrone/YADrone/src/de/yadrone/apps/tutorial at master · MahatmaX/YADrone · GitHub. [ONLINE] Available at: https://github.com/MahatmaX/YADrone/tree/master/YADrone/src/de/yadrone/apps/tutorial. [Accessed 25 September 2014].
     */
    private IARDrone drone;

    public void onCreate()
    {
        drone = new ARDrone("192.168.1.1", null); // null because of missing video support on Android
    }

    public IARDrone getARDrone()
    {
        return drone;
    }
}
