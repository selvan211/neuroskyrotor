package com.msc.arthur.minddrone;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.neurosky.thinkgear.TGDevice;

import de.greenrobot.event.EventBus;
import de.yadrone.base.ARDrone;
import de.yadrone.base.IARDrone;
import de.yadrone.base.navdata.Altitude;
import de.yadrone.base.navdata.AltitudeListener;
import de.yadrone.base.navdata.VelocityListener;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ControlDroneFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ControlDroneFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 *
 * YADrone/YADrone/android/src/de/yadrone at master · MahatmaX/YADrone · GitHub. 2014. YADrone/YADrone/android/src/de/yadrone at master · MahatmaX/YADrone · GitHub. [ONLINE] Available at: https://github.com/MahatmaX/YADrone/tree/master/YADrone/android/src/de/yadrone. [Accessed 25 September 2014].
 *
 * YADrone/YADrone/src/de/yadrone/apps/tutorial at master · MahatmaX/YADrone · GitHub. 2014. YADrone/YADrone/src/de/yadrone/apps/tutorial at master · MahatmaX/YADrone · GitHub. [ONLINE] Available at: https://github.com/MahatmaX/YADrone/tree/master/YADrone/src/de/yadrone/apps/tutorial. [Accessed 25 September 2014].
 *
 */
public class ControlDroneFragment extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    private TextView textViewConnected;
    private IARDrone drone;
    private int attention;
    private int meditation;
    private int blinkCount;
    private boolean isDroneDown = true;
    private ProgressBar progressBarAtt, progressBarMed;
    private TextView blinkText, textAtt,textMed;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ControlDroneFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ControlDroneFragment newInstance(String param1, String param2) {
        ControlDroneFragment fragment = new ControlDroneFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public ControlDroneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        EventBus.getDefault().register(this);

        attention = 0;
        meditation = 0;
        blinkCount = 0;

        progressBarAtt = (ProgressBar)view.findViewById(R.id.progressBar);
        progressBarMed = (ProgressBar)view.findViewById(R.id.progressBar2);
        blinkText = (TextView)view.findViewById(R.id.textView3);
        textAtt = (TextView)view.findViewById(R.id.textView2);
        textMed = (TextView)view.findViewById(R.id.textView);


        //drone.getCommandManager().setMinAltitude(2000);

        //A reduced set of navigation data is sent
        //drone.getCommandManager().setNavDataDemo(true);
        //drone.setSpeed(10);
        //drone.getCommandManager().setMaxVz(30);
        //drone.getCommandManager().setMaxVz(10);

        /*drone.setMinAltitude(800);
        drone.setMaxAltitude(2000);*/

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_control_drone, container, false);

        textViewConnected = (TextView) view.findViewById(R.id.textviewConnected);

        if(drone.getConfigurationManager().isConnected()){
            textViewConnected.setText("Connected, Attention = " + attention);
        }else {
            textViewConnected.setText("Disconnected");
        }

        //Toast.makeText(getActivity().getApplicationContext(), "Touch and hold the buttons", Toast.LENGTH_SHORT).show();
        safeButtons();

        return view;
    }

    private void safeButtons(){

        final Button landing = (Button)view.findViewById(R.id.cmd_landing);
        landing.setOnClickListener(new View.OnClickListener() {
            boolean isFlying = false;
            public void onClick(View v)
            {
                if (!isFlying)
                {
                    drone.takeOff();
                    landing.setText("Landing");
                }
                else
                {
                    drone.landing();
                    landing.setText("Take Off");
                }
                isFlying = !isFlying;
            }
        });

        /*Button emergency = (Button)view.findViewById(R.id.cmd_emergency);
        emergency.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN)
                    drone.getCommandManager().up(20);
                else if (event.getAction() == MotionEvent.ACTION_UP)
                    drone.hover();

                return  true;
            }
        });*/
        Button emergency = (Button)view.findViewById(R.id.cmd_emergency);
        emergency.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                drone.reset();
            }
        });
    }

    private void eegControl(){

        if(attention > 35){
            if(isDroneDown){
                drone.takeOff();
                isDroneDown = !isDroneDown;
            }
        }
        /**
         * Making the drone go up and down depending on the attention
         */


        if(!isDroneDown){
            if(attention>50){
                drone.getCommandManager().up(20);
            }
            if(attention<50){
                drone.getCommandManager().down(20);

            }
        }


        if(!isDroneDown){

            /**
             * Use of blink to make the drone spin
             */
            if(blinkCount>90){
                  //  drone.getCommandManager().spinRight(20);
                //Toast.makeText(getActivity().getApplicationContext(),"Blink above 120", Toast.LENGTH_SHORT).show();
            }

            /**
             * Use of meditation to make drone go right
             */
            /*if(meditation>90){
                drone.getCommandManager().goRight(20);
            }else{
                drone.hover();
            }*/

            /**
             * Use attention to make drone go forward
             */
            if(attention>60){
                drone.getCommandManager().setMaxEulerAngle(0.08f);
                drone.getCommandManager().forward(20);
            }else{
                drone.getCommandManager().hover();
            }
        }


        drone.getNavDataManager().addAltitudeListener(new AltitudeListener() {
            @Override
            public void receivedAltitude(int i) {
//                Log.v("Drone altitude", ": " + i);
                /*if(i<1300){
                    drone.getCommandManager().up(30);

                }else{
                    drone.hover();
                }*/
                //Log.v("Speed", ": " + drone.getSpeed());

  //              if(i>1000){
                    //drone.hover();
                    //drone.getCommandManager().spinRight(200);
                    //drone.getCommandManager().hover();
    //            }
            }

            @Override
            public void receivedExtendedAltitude(Altitude altitude) {

            }
        });

        drone.getNavDataManager().addVelocityListener(new VelocityListener() {
            @Override
            public void velocityChanged(float v, float v2, float v3) {
                //Log.v("Velocity", ": " + v + " , " + v2 + " , " + v3 + " ");
            }
        });

        drone.addSpeedListener(new ARDrone.ISpeedListener() {
            @Override
            public void speedUpdated(int i) {
      //          Log.v("Speed", ": " + i);
            }
        });
    }


    //EventBus onEvent method
    public void onEvent(Message msg){

        switch (msg.what) {
            //Updating the fields with the appropriate cases
            case TGDevice.MSG_ATTENTION:
                attention = msg.arg1;
                progressBarAtt.setProgress(attention);
                textAtt.setText("Attention: " + attention);
                break;
            case TGDevice.MSG_MEDITATION:
                meditation = msg.arg1;
                progressBarMed.setProgress(meditation);
                textMed.setText("Meditation: " + meditation);
                break;
            case TGDevice.MSG_EEG_POWER:
                break;
            case TGDevice.MSG_BLINK:
                blinkCount = msg.arg1;
                blinkText.setText("Blink: " +blinkCount);

            default:
                break;
        }
        //Controlling the drone with the TGDevice specs
        eegControl();

    }

    @Override
    public void onResume() {
        super.onResume();
       // YADroneApplication app = (YADroneApplication)getActivity().getApplication();
       // IARDrone drone = app.getARDrone();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        YADroneApplication app = (YADroneApplication)activity.getApplication();
        drone = app.getARDrone();
        //Log.i("Speed", "" + drone.getSpeed());
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
